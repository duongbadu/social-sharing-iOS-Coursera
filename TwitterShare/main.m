//
//  main.m
//  TwitterShare
//
//  Created by du Duong on 4/26/16.
//  Copyright © 2016 Dat Viet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
