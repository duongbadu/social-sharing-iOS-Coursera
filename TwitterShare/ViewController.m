//
//  ViewController.m
//  TwitterShare
//
//  Created by du Duong on 4/26/16.
//  Copyright © 2016 Dat Viet. All rights reserved.
//

#import "ViewController.h"
#import "Social/Social.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextView *tweetTextView;
@property (weak, nonatomic) IBOutlet UITextView *facebookTextView;
@property (weak, nonatomic) IBOutlet UITextView *moreTextVIew;

- (void) configureShareView;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureShareView];
}

- (void) showAlertMessage: (NSString *) myMessage {
    UIAlertController *alertController;
    alertController = [UIAlertController alertControllerWithTitle:@"SocialShare" message:myMessage preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)showFacebook:(id)sender {
    NSLog(@"Enter: %s", __FUNCTION__);
    
    if ([self.facebookTextView isFirstResponder]) {
        [self.facebookTextView resignFirstResponder];
    }
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *facebookVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebookVC setInitialText:self.facebookTextView.text];
        [self presentViewController:facebookVC animated:YES completion:nil];
    }
    else {
        [self showAlertMessage:@"Please sign in to Facebook"];
    }
}

- (IBAction)showMore:(id)sender {
    NSLog(@"Enter: %s", __FUNCTION__);
    if ([self.moreTextVIew isFirstResponder]) {
        [self.moreTextVIew resignFirstResponder];
    }
    UIActivityViewController *moreVC = [[UIActivityViewController alloc]
                                        initWithActivityItems:@[self.moreTextVIew.text] applicationActivities:nil
                                        ];
    [self presentViewController:moreVC animated:YES completion:nil];
}

- (IBAction)showMsg:(id)sender {
    NSLog(@"Enter: %s", __FUNCTION__);
    [self showAlertMessage:@"This doesn't do anything!"];
}

- (IBAction)showShareAction:(id)sender {
    if ([self.tweetTextView isFirstResponder]) {
        [self.tweetTextView resignFirstResponder];
    }
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *twitterVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        if ([self.tweetTextView.text length] < 140) {
            [twitterVC setInitialText:self.tweetTextView.text];
        }
        else {
            NSString *shortText = [self.tweetTextView.text substringToIndex:140];
            [twitterVC setInitialText:shortText];
        }
        
        [self presentViewController:twitterVC animated:YES completion:nil];
    }
    else {
        [self showAlertMessage:@"Please sign in to Twitter before you tweet"];
    }

    
    UIAlertController *actionController = [UIAlertController alertControllerWithTitle:@"Share" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *tweetAction = [UIAlertAction actionWithTitle:@"Tweet"
                                                           style:UIAlertActionStyleDefault handler:
                                  ^(UIAlertAction* action){
                                      //check for twitter sign in
                                      if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                                          
                                          SLComposeViewController *twitterVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                                          
                                          if ([self.tweetTextView.text length] < 140) {
                                              [twitterVC setInitialText:self.tweetTextView.text];
                                          }
                                          else {
                                              NSString *shortText = [self.tweetTextView.text substringToIndex:140];
                                              [twitterVC setInitialText:shortText];
                                          }
                                          
                                          [self presentViewController:twitterVC animated:YES completion:nil];
                                      }
                                      else {
                                          [self showAlertMessage:@"Please sign in to Twitter before you tweet"];
                                      }
                                  }];
    
    UIAlertAction *facebookAction = [UIAlertAction actionWithTitle:@"Post to Facebook" style:UIAlertActionStyleDefault handler:
                                     ^(UIAlertAction* action){
                                         if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
                                             SLComposeViewController *facebookVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                                             [facebookVC setInitialText:self.tweetTextView.text];
                                             [self presentViewController:facebookVC animated:YES completion:nil];
                                         }
                                         else {
                                             [self showAlertMessage:@"Please sign in to Facebook"];
                                         }
                                     }];
    
    UIAlertAction *moreAction = [UIAlertAction actionWithTitle:@"More" style:UIAlertActionStyleDefault handler:
                                     ^(UIAlertAction* action){
                                         UIActivityViewController *moreVC = [[UIActivityViewController alloc]
                                                                             initWithActivityItems:@[self.tweetTextView.text] applicationActivities:nil
                                                                             ];
                                         [self presentViewController:moreVC animated:YES completion:nil];
                                     }];
    
    [actionController addAction:tweetAction];
    [actionController addAction:facebookAction];
    [actionController addAction:moreAction];
    [actionController addAction:cancelAction];
    
//    [self presentViewController:actionController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) configureShareView
{
    self.tweetTextView.layer.cornerRadius = 10.0;
    self.tweetTextView.layer.borderColor = [UIColor colorWithWhite:0 alpha:0.5].CGColor;
    self.tweetTextView.layer.borderWidth = 2.0;
    
    self.facebookTextView.layer.cornerRadius = 10.0;
    self.facebookTextView.layer.borderColor = [UIColor colorWithWhite:0 alpha:0.5].CGColor;
    self.facebookTextView.layer.borderWidth = 2.0;
    
    self.moreTextVIew.layer.cornerRadius = 10.0;
    self.moreTextVIew.layer.borderColor = [UIColor colorWithWhite:0 alpha:0.5].CGColor;
    self.moreTextVIew.layer.borderWidth = 2.0;
}

@end
